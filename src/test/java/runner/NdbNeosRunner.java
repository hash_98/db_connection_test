package runner;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import utilities.PropertyFileHandler;

@RunWith(Cucumber.class)

@CucumberOptions(features = { "src/resource/Features/VerifyOTP.feature" }, glue = {
		
		
		"com/epic/ndb/StepDefinition/" })


public class NdbNeosRunner {
	@BeforeClass
	public static void setup() throws FileNotFoundException, IOException {

		PropertyFileHandler.getPropertyFileHandler();
		PropertyFileHandler.loadPropertyFile("src/resources/TestData/data.properties");

	}
	
}