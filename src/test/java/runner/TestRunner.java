package runner;

import encrypter.MobileEncrypter;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import utilities.PropertyFileHandler;

@RunWith(Cucumber.class)

@CucumberOptions(
		features = "src/resource/Features/VerifyOTP.feature", 
		glue = "stepDefinition",
		strict = true)


public class TestRunner {
	
	@BeforeClass
	public static void setup() throws Exception {
		MobileEncrypter.init();

		PropertyFileHandler.getPropertyFileHandler();
		PropertyFileHandler.loadPropertyFile("src/resources/TestData/data.properties");

	}

}