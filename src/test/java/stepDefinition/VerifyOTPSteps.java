package stepDefinition;

import java.io.IOException;
import java.sql.SQLException;

import base.DatabaseConnection;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;
import utilities.PropertyFileHandler;

public class VerifyOTPSteps {

    RequestSpecification requestSpecification;
    Response response;
    Scenario scenario;

    public String reOTP;

    //Hooks
    @Before
    public void beforeHook (Scenario s){
        this.scenario = s;
    }


    @After
    public void afterHook (Scenario s){
        this.scenario = s;
        if (response == null){
            scenario.write("No Response Received");
        }else {
            scenario.write("Response: " + response.asString());
        }
    }

    @Given("Connect to database and get {string}")
    public void connect_to_database_and_get(String string) throws SQLException, IOException{

        reOTP = DatabaseConnection.fetchDataFromDB(string);

    }

    @Given("Save OTP in Property file")
    public void save_OTP_in_property_file() {
        PropertyFileHandler.writeProperty("CheckingOTP", reOTP);
        System.out.println("CheckingOTP : " + reOTP);
    }

    @Then("Check if OTP is valid")
    public void check_if_OTP_is_valid() {
        String expected = PropertyFileHandler.readProperty("CheckingOTP");
        System.out.println("Verifying OTP : " + reOTP + " = " + expected);
        Assert.assertEquals(reOTP, expected);
    }
}
