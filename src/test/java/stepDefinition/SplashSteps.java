package stepDefinition;


import base.BaseClass;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import utilities.PropertyFileHandler;
import utilities.RequestEncryptionHandler;

import java.util.Iterator;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;


public class SplashSteps extends BaseClass {
    Scenario scenario;

    //Hooks
    @Before
    public void beforeHook (Scenario s){
        this.scenario = s;
    }

//    @After
//    public void afterHook (Scenario s){
//        this.scenario = s;
//        if (response == null){
//            scenario.write("No Response Received");
//        }else {
//            scenario.write("Response: " + response.asString());
//        }
//    }


    @Given("LVT API is up and running")
	public void lvt_API_is_up_and_running() {
	    RequestEncryptionHandler.getRequestEncryptionHandler();
		requestSpecification = given().baseUri(baseURL);
	}

	@When("Set JSON body and header")
	public void set_JSON_body_and_header() throws Exception {

        headerJson.put("txnAquirerBankCode", "");
        headerJson.put("deviceId", 0);
        headerJson.put("msgFormatVersion", msgFormatVersion);
        headerJson.put("pushId", pushId = getPushId());
        headerJson.put("ticket", ticket);
        headerJson.put("traceNo", traceNo = getTraceNo());
        headerJson.put("txnDateTime", txnDateTime = getTransactionDateTime());
        headerJson.put("txnType", PropertyFileHandler.readProperty("MOBILE_APP_KEY_VERIFICATION"));
        headerJson.put("osType", osType = "Android");
        headerJson.put("mobVersion", mobVersion = 157);

        bodyJson.put("merchantType", 2);
        bodyJson.put("key", "AAAAB0ylexb58eo+");
        bodyJson.put("osType", "Android");
        bodyJson.put("mobVersion", 157);

        requestParameters.put("body", bodyJson);
        requestParameters.put("header", headerJson);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyPrint = gson.toJson(requestParameters);
        scenario.write(">>>>> Request Parameters: " + "\n" + prettyPrint + " >>>>>");

        message = RequestEncryptionHandler.EncryptRequest(requestParameters);
        response = given().relaxedHTTPSValidation().contentType("application/json").body(message).when().post(baseURI);
	}

    @Then("Get and Decrypt Response")
    public void get_and_decrypt_response() throws Exception {
        JsonPath jsonPath = response.jsonPath();
        message = jsonPath.get("data");
        responseJson = RequestEncryptionHandler.DecryptResponse(message);
        resJson= new JsonPath(String.valueOf(responseJson));
    }

	@Then("API returns the response with status code as {int}")
	public void api_returns_the_response_with_status_code_as(Integer int1) {
        //response.then().assertThat().statusCode(HttpStatus.SC_OK);
        response.then().assertThat().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Then("Validate header params")
	public void validate_header_params() {
//        for(Iterator iterator = headerJson.keySet().iterator(); iterator.hasNext();) {
//            String key = (String) iterator.next();
//            if(resJson.get("header." + key).equals("mobVersion") || resJson.get("header." + key).equals("osType")){
//
//            }else {
//                if(!(headerJson.get(key).equals(resJson.get("header." + key)))){
//                    System.out.println("[" + key + "]" + ":" +headerJson.get(key) + " -----> " + resJson.get("header." + key));
//                    Assert.assertEquals(headerJson.get(key), resJson.get("header." + key));
//                }
//            }
//        }
	}
}
