package base;

import java.sql.SQLException;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.junit.Assert;
import utilities.PropertyFileHandler;
import utilities.RequestEncryptionHandler;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class BaseClass {

//	public static void main(String[] args) throws SQLException, IOException {
//    	PropertyFileHandler.getPropertyFileHandler();
//    	PropertyFileHandler.loadPropertyFile("src/resource/TestData/data.properties");
//
//		String fieldValue = "OTP";
//
//		String reOTP = DatabaseConnection.fetchDataFromDB(fieldValue);
//		PropertyFileHandler.writeProperty("CheckingOTP", reOTP);
//		System.out.println("CheckingOTP : " + reOTP);
//	}

	public static String baseURL = "https://epictechdev.com/eswitch/doProcess";

	public static RequestSpecification requestSpecification;
	public static Response response;

	public static JSONObject bodyJson = new JSONObject();
	public static JSONObject headerJson = new JSONObject();

	public static JSONObject requestParameters = new JSONObject();
	public static JSONObject finalJsonBody = new JSONObject();
	public static String message;

	public static JSONObject responseJson = new JSONObject();
	public static JsonPath resJson = new JsonPath(String.valueOf(responseJson));

	//header Fields
	public static String txnAquirerBankCode;
	public static String deviceId;
	public static String msgFormatVersion = "001";
	public static String pushId;
	public static String ticket = "00011";
	public static String traceNo = "111111";
	public static String txnDateTime;

	//body Fields
	public static int merchantType;
	public static String key;
	public static String osType;
	public static int mobVersion;
	public static String mobileNo;
	public static String nic;
	public static String OTP;
	public static String mpinNew;
	public static String mpin;

	public static String getTraceNo() {
		if (null != traceNo || !traceNo.trim().equals("")) {
			if (traceNo.equals("999999")) {
				traceNo = "1";
			} else {
				int trace = Integer.parseInt(traceNo);
				trace++;
				traceNo = String.valueOf(trace);
			}
		} else {
			traceNo = "1";
		}
		return traceNo = zeroPad(6);
	}

	public static String zeroPad(int length) {
		try {
			StringBuffer sb = new StringBuffer();

			if(traceNo.length() > length){
				sb.append(traceNo.substring(0, length));
			}
			else {
				for (int i = 0; i < (length - traceNo.length()); i++) {
					sb.append("0");
				}
			}

			return (sb + traceNo);
		} catch (Exception e) {
			return null;
		}
	}

	public static String getTransactionDateTime() {
		DateFormat df = new SimpleDateFormat("yyyyMMddkkmmss");
		Calendar c = Calendar.getInstance();
		Date day = c.getTime();
		String s2 = df.format(day);

		return s2;
	}

	public static String getPushId() {
		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder returnString = new StringBuilder(170);

		for (int i = 0; i < 170; i++) {
			// generate a random number
			int index = (int)(AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			returnString.append(AlphaNumericString.charAt(index));
		}

		return returnString.toString();
	}

	public static String getEmail() {
		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder returnString = new StringBuilder(15);

		for (int i = 0; i < 15; i++) {
			// generate a random number
			int index = (int)(AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			returnString.append(AlphaNumericString.charAt(index));
		}

		return (returnString.toString() + "@gmail.com");
	}

	public static String getNIC() {

		int increment = Integer.parseInt(nic);
		increment++;
		nic = String.valueOf(increment);

		return nic;
	}

	public static String getMobileNumber() {

		int increment = Integer.parseInt(mobileNo);
		increment++;
		mobileNo = String.valueOf(increment);

		return mobileNo;
	}

	public static String getFirstName() {
		// chose a Character random from this String
		String NumericString = "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder returnString = new StringBuilder(10);

		for (int i = 0; i < 10; i++) {
			// generate a random number
			int index = (int)(NumericString.length() * Math.random());

			// add Character one by one in end of sb
			returnString.append(NumericString.charAt(index));
		}

		return returnString.toString();
	}

	public static String getLastName() {
		// chose a Character random from this String
		String NumericString = "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder returnString = new StringBuilder(15);

		for (int i = 0; i < 15; i++) {
			// generate a random number
			int index = (int)(NumericString.length() * Math.random());

			// add Character one by one in end of sb
			returnString.append(NumericString.charAt(index));
		}

		return returnString.toString();
	}

	public static String getUUID() {
		String uuid = UUID.randomUUID().toString().replace("-", "");
		return  uuid;
	}

	public void SignUpResponseHeaderValidator() throws Exception {
		response = given().relaxedHTTPSValidation().contentType("application/json").body(message).when().post(baseURI).then().log().all()
				.assertThat().statusCode(HttpStatus.SC_OK).extract().response();

		JsonPath jsonPath = response.jsonPath();
		message = jsonPath.get("data");
		responseJson = RequestEncryptionHandler.DecryptResponse(message);
		resJson= new JsonPath(String.valueOf(responseJson));

		for(Iterator iterator = headerJson.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if(resJson.get("header." + key).equals("mobVersion") || resJson.get("header." + key).equals("osType")){

			}else {
				if(!(headerJson.get(key).equals(resJson.get("header." + key)))){
					System.out.println("[" + key + "]" + ":" +headerJson.get(key) + " -----> " + resJson.get("header." + key));
					Assert.assertEquals(headerJson.get(key), resJson.get("header." + key));
				}
			}
		}
	}
}
