package base;

import java.sql.*;

import utilities.PropertyFileHandler;

import java.io.IOException;

public class DatabaseConnection {
    private static Connection connection = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;

    private static String returnValue;

    public static String fetchDataFromDB (String fieldValue) throws SQLException, IOException {
    	
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@www.epictechdev.com:50150:ceft", "LVMT_DEV2", "password");
            System.out.println("Connected to DataBase");
        } catch (ClassNotFoundException e) {
            System.out.println("Failed to connect");
            e.printStackTrace();
        }
        

        System.out.println(PropertyFileHandler.readProperty(fieldValue));
        
        statement = connection.createStatement();
        resultSet = statement.executeQuery(PropertyFileHandler.readProperty(fieldValue));
    	
        if(resultSet.next()){
        	returnValue = resultSet.getString(fieldValue);
            System.out.println(returnValue);  
        }
        return returnValue;
    }
}
