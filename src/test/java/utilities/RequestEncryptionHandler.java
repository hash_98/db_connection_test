package utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jpos.iso.ISOUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import encrypter.MobileEncrypter;

public class RequestEncryptionHandler {

    static RequestEncryptionHandler requestEncryptionHandler;

    private RequestEncryptionHandler() {

    }

    public static RequestEncryptionHandler getRequestEncryptionHandler() {
        if (requestEncryptionHandler == null) {
            requestEncryptionHandler = new RequestEncryptionHandler();
        }
        return requestEncryptionHandler;
    }

    public static String EncryptRequest(JSONObject requestParameters) throws Exception {
        JSONObject finalJsonBody = new JSONObject();
        String message;

        byte[] request = requestParameters.toJSONString().getBytes();
        byte[] enRequest = MobileEncrypter.packet_encrypte(request, true);
        finalJsonBody.put("data", ISOUtil.hexString(enRequest));

        message = finalJsonBody.toJSONString();
        System.out.println("Test Test:  " + message);
        return message;
    }

    public static JSONObject DecryptResponse(String message) throws Exception {
        String prettyPrint;
        byte[] decryptResponse;
        decryptResponse = MobileEncrypter.packet_decrypte(ISOUtil.hex2byte(message));
        assert decryptResponse != null;
        String responseString = new String(decryptResponse);
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(responseString);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        prettyPrint = gson.toJson(json);
        System.out.println("<<<<< Response Parameters: " + "\n" + prettyPrint + " <<<<<");
        return json;
    }
}
