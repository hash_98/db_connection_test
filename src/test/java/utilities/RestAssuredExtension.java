package utilities;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

public class RestAssuredExtension {
	
	public static RequestSpecification Request;
	
	//Arrange
	public RestAssuredExtension() {
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setBaseUri("https://epictechdev.com/eswitch/doProcess");
		builder.setContentType("application/json");
		RequestSpecification requestSpec = builder.build();
		Request = RestAssured.given().spec(requestSpec);
	}
	
	//Act
	public ResponseOptions<Response> PostAct() {
		return null;
	}

}
