package security;


import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;

public class DataSecurity {

    public static byte[] getSHA1Bytes(String str) throws Exception {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] sh1 = sha1.digest(str.getBytes());

        return sh1;
    }

    public static String getSHA1(String str) throws Exception {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] sh1 = sha1.digest(str.getBytes());

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < sh1.length; i++) {
            //System.out.println(String.format("%02X", sh1[i]));
            sb.append(String.format("%02X", sh1[i]));
        }

        return sb.toString();
    }

    public static byte[] createDataSecurityKey(byte[] pushID) throws Exception {
        byte[] zeros = {0x00, 0x00, 0x00, 0x00};

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(pushID);
        outputStream.write(zeros);

        return outputStream.toByteArray();
    }

    public static byte[] getSHA256Bytes(String str) throws Exception {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] sh256 = sha256.digest(str.getBytes("UTF-8"));

        return sh256;
    }

    public static String getSHA256(String str) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(str.getBytes("UTF-8"));

        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            if ((0xff & digest[i]) < 0x10) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(0xff & digest[i]));
        }

        return sb.toString();
    }

    public static String getKey(String str) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(str.getBytes("UTF-8"));

        byte[] digest = md.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String output = bigInt.toString().substring(0, 16);

        return output;
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String getHashPassword(String text, String saltValue) {
        try {
            int iterations = 10;
            int keyLength = 512;
            char[] passwordChars = getHashedPassword256(text).toCharArray();
            byte[] saltBytes = saltValue.getBytes();
            byte[] result = hashPassword(passwordChars, saltBytes, iterations, keyLength);
            return String.format("%0" + result.length * 2 + 'x', new BigInteger(1, result));
        } catch (Exception e) {
            return getHashedPassword256(text);
        }
    }

    public static String getHashPasswordOld(String text, String saltValue) {
        try {
            int iterations = 10;
            int keyLength = 512;
            char[] passwordChars = getHashedPassword(text).toCharArray();
            byte[] saltBytes = saltValue.getBytes();
            byte[] result = hashPassword(passwordChars, saltBytes, iterations, keyLength);
            return String.format("%0" + result.length * 2 + 'x', new BigInteger(1, result));
        } catch (Exception e) {
            return getHashedPassword(text);
        }
    }

    private static byte[] hashPassword(final char[] password, final byte[] salt, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
            SecretKey key = skf.generateSecret(spec);
            return key.getEncoded();
        } catch (Exception e) {
            return new byte[]{};
        }
    }

    private static String getHashedPassword(String plainPassword) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] textBytes = plainPassword.getBytes("iso-8859-1");
            md.update(textBytes, 0, textBytes.length);
            byte[] sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (Exception e) {
            return "";
        }
    }

    private static String getHashedPassword256(String plainPassword) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] textBytes = plainPassword.getBytes("iso-8859-1");
            md.update(textBytes, 0, textBytes.length);
            byte[] sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (Exception e) {
            return "";
        }
    }

    public static String getEncrypt(String key, String value) throws Exception {

        TEA tea = new TEA(key.getBytes());
        String encVal = tea.getEncrypt(value);

        return encVal;
    }

    public static String getDecrypt(String key, String value) throws Exception {

        TEA tea = new TEA(key.getBytes());
        String encVal = tea.getDecrypt(value);

        return encVal;
    }
}
