package encrypter;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jpos.iso.ISODate;
import org.jpos.iso.ISOUtil;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Provider;
import java.security.Security;
import java.util.Arrays;
import java.util.Date;


public class MobileEncrypter {


    private static Provider PROVIDER = null;
    private static SecretKey BDK = null;
    private static String TRF_ECB = "DESede/ECB/NoPadding";
    private static String TRF_PCKS5 = "DESede/CBC/PKCS5Padding";
    private static byte[] IV = new byte[8];
    private static byte[] KVC = new byte[16];
    private static int TC = 0;


    public static void init() throws Exception {

        PROVIDER = new BouncyCastleProvider();
        Security.addProvider(PROVIDER);
        BDK = new SecretKeySpec(new byte[16], "DESede");

    }

    public static byte[] packet_encrypte(byte[] data, boolean enc_status) throws Exception {

        String TC = getTC();

        if (enc_status) {
            String UK = ISOUtil.padright(TC, 32, 'F');
            byte KEY[] = encrypt(BDK, ISOUtil.hex2byte(UK), "ECB", null, TRF_ECB);

            SecretKey sk = new SecretKeySpec(KEY, "DESede");
            byte KVC[] = createKVC(sk, TRF_ECB);
            byte encr_byte[] = encrypt(sk, data, "CBC", IV, TRF_PCKS5);
            TC = TC + "01" + ISOUtil.hexString(KVC);
            return ISOUtil.concat(ISOUtil.hex2byte(TC), encr_byte);

        } else {
            TC = TC + "02000000";
            return ISOUtil.concat(ISOUtil.hex2byte(TC), data);
        }
    }

    public static byte[] packet_decrypte(byte[] data) throws Exception {

        byte TC[] = new byte[3];
        byte KVC[] = new byte[3];
        byte DTA[] = new byte[data.length - 7];
        byte DTA2[] = new byte[data.length];
        byte MODE = data[3];

        System.arraycopy(data, 0, TC, 0, 3);
        System.arraycopy(data, 4, KVC, 0, 3);
        System.arraycopy(data, 7, DTA, 0, DTA.length);
        System.arraycopy(data, 0, DTA2, 0, DTA2.length); //Comment According to E Switch configurations.

        if (MODE == 1) {

            String UK = ISOUtil.padright(ISOUtil.hexString(TC), 32, 'F');
            byte KEY[] = encrypt(BDK, ISOUtil.hex2byte(UK), "ECB", null, TRF_ECB);
            SecretKey sk = new SecretKeySpec(KEY, "DESede");
            byte GEN_KVC[] = createKVC(sk, TRF_ECB);

            if (Arrays.equals(KVC, GEN_KVC)) {
                byte decr_byte[] = decrypt(sk, DTA, "CBC", IV, TRF_PCKS5);
                return decr_byte;
            } else {
                return null;
            }
        } else {
//            return DTA; //Uncomment According to E Switch configurations.
            return DTA2;
        }
    }

    private static String getTC() throws Exception {

        return ISODate.getTime(new Date());
    }

    private static byte[] encrypt(SecretKey secretKey, byte[] clearBytes,
                                  String mode, byte[] IV, String tf) throws Exception {

        if ("CBC".equals(mode)) {
            Cipher cipher = Cipher.getInstance(tf, PROVIDER
                    .getName());
            cipher
                    .init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(
                            IV));
            return cipher.doFinal(clearBytes);
        } else {
            Cipher cipher = Cipher.getInstance(tf, PROVIDER
                    .getName());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(clearBytes);
        }

    }

    private static byte[] decrypt(SecretKey secretKey, byte[] ciperBytes,
                                  String mode, byte[] IV, String tf) throws Exception {

        if ("CBC".equals(mode)) {
            Cipher cipher = Cipher.getInstance(tf, PROVIDER
                    .getName());
            cipher
                    .init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(
                            IV));

            return cipher.doFinal(ciperBytes);
        } else {
            Cipher cipher = Cipher.getInstance(tf, PROVIDER
                    .getName());
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            return cipher.doFinal(ciperBytes);
        }
    }

    private static byte[] createKVC(SecretKey skey, String tf) throws Exception {
        byte kvc[] = encrypt(skey, KVC, "ECB", null, tf);
        return ISOUtil.hex2byte(ISOUtil.hexString(kvc, 0, 3));
    }
}
