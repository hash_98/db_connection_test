
Feature: Splash Request 

  Scenario: With User Splash Request
    Given LVT API is up and running
    When Set JSON body and header
    Then Get and Decrypt Response
    And API returns the response with status code as 200
    And Validate header params

